PREFIX?=/usr/local
INSTALL_LOC=$(DESTDIR)$(PREFIX)

.PHONY: all install
all: weer_client

weer_client:
	cargo build --release

install: install-resources
	cargo install --path . --root $(INSTALL_LOC)
	build-aux/post_install.py

install-resources:
	$(MAKE) -C data PREFIX=$(PREFIX) DESTDIR=$(DESTDIR)
