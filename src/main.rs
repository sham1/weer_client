extern crate gtk;
extern crate gio;

extern crate futures;
extern crate tokio;
extern crate multiqueue;

extern crate weerclient;

use gtk::prelude::*;
use gio::prelude::*;

use gio::SettingsExt;

use tokio::prelude::*;

use std::env::args;

use std::boxed::Box;

use std::sync::Arc;
use std::thread;

use std::time::{Instant, Duration};

use std::sync::atomic::{AtomicUsize, Ordering};

use futures::sync::mpsc;
use futures::{Sink, Stream};
use tokio::timer::Interval;
use futures::future::Either;

use std::net::ToSocketAddrs;

use std::io::{Error, ErrorKind};

use weerclient::{RelayMessage, ClientCommandBuilder};

const GLADE_SRC: &'static str = include_str!("Weerclient.glade");

#[derive(Clone)]
struct GuiEventSender(std::sync::mpsc::Sender<Box<Fn(&mut GuiState) + Send>>);
impl GuiEventSender {
    fn send<F>(&self, f: F)
    where
        F: Fn(&mut GuiState) + Send + Sync + 'static,
    {
        self.0.send(Box::new(f)).expect("Did the GTK thread die?");
    }
}

#[derive(Clone)]
struct GuiState {
    pub buffers: Vec<(String, gtk::TextBuffer)>,
    pub buf_view: gtk::TextView,
}

struct ConnDetails {
    pub hostname: String,
    pub port: u16,
    pub tls: bool,
    pub password: String,
}

type StateUI = (gtk::Widget, gtk::HeaderBar);
type StateStacks = (gtk::Stack, gtk::Stack);

fn build_chat_ui(builder: &gtk::Builder) -> mpsc::Sender<ConnDetails> {

    let buffer_list_toggle_button: gtk::ToggleButton = builder.get_object("ShowBuffers").unwrap();
    let user_list_toggle_button: gtk::ToggleButton = builder.get_object("ShowUsers").unwrap();

    let user_list_revealer: gtk::Revealer = builder.get_object("UserViewRevealer").unwrap();
    let buffer_list_revealer: gtk::Revealer = builder.get_object("BufferViewRevealer").unwrap();

    buffer_list_toggle_button.connect_toggled(move |_| {
        buffer_list_revealer.set_reveal_child(!buffer_list_revealer.get_reveal_child());
    });

    user_list_toggle_button.connect_toggled(move |_| {
        user_list_revealer.set_reveal_child(!user_list_revealer.get_reveal_child());
    });

    let buffer_view: gtk::TreeStore = builder.get_object("BufferView").unwrap();
    let buffer_column: gtk::TreeViewColumn = builder.get_object("BufferName").unwrap();
    let cell_renderer: gtk::CellRendererText = gtk::CellRendererText::new();
    buffer_column.pack_start(&cell_renderer, true);
    buffer_column.add_attribute(&cell_renderer, "text", 0);

    let (tx, rx) = multiqueue::mpmc_fut_queue::<weerclient::RelayMessage>(1);
    let (cb_tx, cb_rx) = std::sync::mpsc::channel::<Box<Fn(&mut GuiState) + Send>>();

    let buffer_content_view: gtk::TextView = builder.get_object("BufferContentsView").unwrap();

    let tx_copy = tx.clone();
    let msg_box: gtk::Entry = builder.get_object("MsgBox").unwrap();
    msg_box.connect_activate(move |entry| if let Some(txt) = entry.get_text() {
        let cmd = weerclient::RelayMessage::ClientMessage(format!("{}\n", txt));
        tx_copy.clone().send(cmd).wait();
        entry.set_text("");
    });

    let mut state = GuiState {
        buffers: Vec::new(),
        buf_view: buffer_content_view,
    };
    gtk::timeout_add(50, move || {
        cb_rx.try_recv().map(|cb| cb(&mut state.clone()));
        gtk::Continue(true)
    });

    let gui_event_sender: GuiEventSender = GuiEventSender(cb_tx);
    let (conn_tx, conn_rx) = mpsc::channel::<ConnDetails>(1);
    thread::spawn(move || {
        let conn_client =
            conn_rx.for_each(move |details| {
                let pong_misses = Arc::new(AtomicUsize::new(0));
                let rx = rx.clone();
                let tx = tx.clone();
                let gui_event_sender = gui_event_sender.clone();
                let hostname = details.hostname;
                let port = details.port;
                let password = details.password;
                let addr = (hostname.as_str(), port)
                    .to_socket_addrs()
                    .unwrap()
                    .next()
                    .unwrap();
                let conn = tokio::net::TcpStream::connect(&addr);
                // TODO: Make this work with TLS
                let codec = weerclient::RelayCodec::new();
                let conn = conn.map(|sock| {
                    sock.framed(codec).split()
                }).and_then(move |(to_server, from_server)| {
                    let init_cmd = ClientCommandBuilder::new()
                        .new_command("init")
                        .add_command_arg(format!("password={}", password))
                        .build();

                    to_server.send(init_cmd)
                        .and_then(|s| s.flush())
                        .and_then(move |s| Ok((s, from_server)))
                }).and_then(|(to_server, from_server)| {
                    let initial_buffers_cmd = ClientCommandBuilder::new()
                        .new_command("hdata")
                        .add_command_arg("buffer:gui_buffers(*)")
                        .add_command_arg("full_name,short_name,number,title")
                        .build();

                    to_server.send(initial_buffers_cmd)
                        .and_then(|s| s.flush())
                        .and_then(move |s| Ok((s, from_server)))
                });
                conn.and_then(move |(to_server, from_server)| {
                    let mut response_handlers:
                    Vec<(String,
                        Arc<Box<Fn(RelayMessage, GuiEventSender) -> bool
                            + Send + Sync>>)>
                    = Vec::new();
                    let pong_misses_copy = pong_misses.clone();
                    let pong_handler: Arc<
                        Box<
                            Fn(RelayMessage,
                               GuiEventSender)
                               -> bool
                                + Send
                                + Sync,
                        >,
                    > = Arc::new(Box::new(move |_, _| {
                        pong_misses_copy.clone().store(0, Ordering::Relaxed);
                        true
                    }));
                    let writer = rx
                        .map_err(|()| unreachable!("rx cannot fail!"))
                        .fold(to_server, |to_server, msg| {
                            to_server.send(msg)
                         })
                         .map(|_| ());
                    response_handlers.push(("_pong".to_string(), pong_handler));
                    let reader =
                        from_server.for_each(move |obj| {
                            let default_handler: Arc<Box<Fn(RelayMessage,
                        GuiEventSender) -> bool + Sync + Send>> =
                    Arc::new(Box::new(|obj, gui| {
                        println!("{:?}", obj);
                        true
                    }));
                            let obj_copy = obj.clone();
                            let resp_handler = response_handlers
                                .clone()
                                .into_iter()
                                .enumerate()
                                .find(move |(_, (n, _))| match obj_copy.clone() {
                                    RelayMessage::ClientMessage(_) => unreachable!(),
                                    RelayMessage::RelayMessage { id, .. } => {
                                        if let Some(id) = id { *n == id } else { false }
                                    }
                                })
                                .map(|(id, (_, func))| (id, func))
                                .unwrap_or((0, default_handler));
                            let (id, func) = resp_handler;
                            let should_preserve = func(obj.clone(), gui_event_sender.clone());
                            if !should_preserve {
                                response_handlers.remove(id);
                            }
                            Ok(())
                        });
                    reader.select(writer).map(|_| ()).map_err(|(err, _)| err)
                }).or_else(|err| {
                        println!("{:?}", err);
                        Ok(())
                    })
            });
        tokio::run(conn_client);
    });
    conn_tx
}

fn main() {
    let application = gtk::Application::new(
        "fi.sinervo.sham1.WeerClient",
        gio::ApplicationFlags::empty(),
    ).expect("Initializing failed");
    application.connect_activate(move |app| {
        let builder = gtk::Builder::new_from_string(GLADE_SRC);
        let settings: gio::Settings = gio::Settings::new("fi.sinervo.sham1.WeerClient");
        let should_connect = settings.get_boolean("relay-has-been-added");
        let content_stack: gtk::Stack = builder.get_object("ContentStack").unwrap();
        let headerbar_stack: gtk::Stack = builder.get_object("HeaderbarStack").unwrap();

        let stacks = (content_stack, headerbar_stack);

        let win: gtk::ApplicationWindow = builder.get_object("MainWindow").unwrap();
        win.set_application(app);

        let initial_setup_ui: StateUI = (
            builder.get_object("InitialSetupUI").unwrap(),
            builder.get_object("InitialSetupHeaderBar").unwrap(),
        );
        let relay_setup_ui: StateUI = (
            builder.get_object("RelaySetupUI").unwrap(),
            builder.get_object("RelaySetupHeaderBar").unwrap(),
        );
        let chat_ui: StateUI = (
            builder.get_object("ChatUI").unwrap(),
            builder.get_object("ChatHeaderBar").unwrap(),
        );

        let conn_tx = build_chat_ui(&builder);
        build_initial_ui(&builder, stacks.clone(), relay_setup_ui);
        build_setup_ui(
            &builder,
            stacks.clone(),
            initial_setup_ui,
            chat_ui.clone(),
            conn_tx,
        );

        if should_connect {
            set_state_ui(stacks.clone(), chat_ui.clone());
            // TODO: handle case where connection details
            //       already exist
        }
        win.show_all();
    });

    application.run(&args().collect::<Vec<_>>());
}

fn build_initial_ui(builder: &gtk::Builder, stacks: StateStacks, relay_setup_ui: StateUI) {
    let setup_button: gtk::Button = builder.get_object("InitialSetupAddRelayButton").unwrap();
    setup_button.connect_clicked(move |_| {
        set_state_ui(stacks.clone(), relay_setup_ui.clone());
    });
}

fn build_setup_ui(
    builder: &gtk::Builder,
    stacks: StateStacks,

    initial_setup_ui: StateUI,
    chat_ui: StateUI,
    conn_tx: mpsc::Sender<ConnDetails>,
) {

    let accept_button: gtk::Button = builder.get_object("RelaySetupAcceptButton").unwrap();
    let cancel_button: gtk::Button = builder.get_object("RelaySetupCancelButton").unwrap();

    let hostname_entry: gtk::Entry = builder.get_object("RelaySetupHostnameEntry").unwrap();
    let port_entry: gtk::Entry = builder.get_object("RelaySetupPortEntry").unwrap();
    let tls_switch: gtk::Switch = builder.get_object("RelaySetupTLSSwitch").unwrap();
    let password_entry: gtk::Entry = builder.get_object("RelaySetupPasswordEntry").unwrap();

    let stacks_copy = stacks.clone();

    cancel_button.connect_clicked(move |_| {
        set_state_ui(stacks_copy.clone(), initial_setup_ui.clone());
    });

    accept_button.connect_clicked(move |_| {
        let mut details = ConnDetails {
            hostname: "".to_string(),
            port: 0,
            tls: false,
            password: "".to_string(),
        };

        let mut is_ready = false;
        let hostname_opt = hostname_entry.get_text().filter(|s| !s.is_empty());
        let port_opt = port_entry
            .get_text()
            .and_then(|p| p.parse::<u16>().ok())
            .filter(|p| *p > 0);
        let password_opt = password_entry.get_text().filter(|s| !s.is_empty());
        if let Some(hostname) = hostname_opt {
            is_ready = true;
            details.hostname = hostname.clone();
            hostname_entry.get_style_context().map(|cxt| {
                cxt.remove_class("error")
            });
        } else {
            hostname_entry.get_style_context().map(|cxt| {
                cxt.add_class("error")
            });
        }
        if let Some(port) = port_opt {
            details.port = port;
            port_entry.get_style_context().map(|cxt| {
                cxt.remove_class("error")
            });
        } else {
            port_entry.get_style_context().map(
                |cxt| cxt.add_class("error"),
            );
            is_ready = false;
        }
        details.tls = tls_switch.get_active();
        if let Some(password) = password_opt {
            details.password = password;
            password_entry.get_style_context().map(|cxt| {
                cxt.remove_class("error")
            });
        } else {
            password_entry.get_style_context().map(|cxt| {
                cxt.add_class("error")
            });
            is_ready = false;
        }
        if is_ready {
            set_state_ui(stacks.clone(), chat_ui.clone());
            conn_tx.clone().try_send(details);
        }
    });
}

fn set_state_ui(stacks: StateStacks, ui: StateUI) {
    stacks.0.set_visible_child(&ui.0);
    stacks.1.set_visible_child(&ui.1);
}
