extern crate tokio_codec;
extern crate bytes;
extern crate libflate;

use tokio_codec::{Encoder, Decoder};
use std::io::{Error, ErrorKind, Read};
use std::boxed::Box;
use bytes::{BytesMut, IntoBuf, Buf, BigEndian};
use libflate::zlib;

#[derive(Clone, Debug)]
pub enum RelayMessage {
    /// This is a message sent from client to relay.
    ClientMessage(String),

    /// This is a message sent from relay to client.
    RelayMessage {
        id: Option<String>,
        objs: Vec<MsgObj>,
    },
}

#[derive(Clone, Debug)]
pub struct HdataObj {
    /// The contained value will always be
    /// MsgObj::Pointer
    pub ppath: Vec<Box<MsgObj>>,

    pub values: Vec<(String, Box<MsgObj>)>,
}

#[derive(Clone, Debug)]
pub enum MsgObj {
    Char(char),
    Int(i32),
    LongInt(i64),
    String(Option<String>),
    Buffer(Option<String>),
    Pointer(Option<String>),
    Time(String),
    Hashtable { pairs: Vec<(Box<MsgObj>, Box<MsgObj>)>, },
    Hdata {
        hpath: Option<String>,
        objs: Vec<HdataObj>,
    },
    Info(String, String),
    Infolist {
        name: String,
        items: Vec<Vec<(String, Box<MsgObj>)>>,
    },
    Array(Option<Vec<Box<MsgObj>>>),
}

#[derive(Clone, Copy)]
enum ObjType {
    Char,
    Int,
    LongInt,
    String,
    Buffer,
    Pointer,
    Time,
    Hashtable,
    Hdata,
    Info,
    Infolist,
    Array,
}

#[derive(Clone, Debug)]
pub struct RelayCodec {
    payload_len: usize,
    is_lzip: bool,

    knows_pl_len: bool,
    knows_compression: bool,
}

impl RelayCodec {
    pub fn new() -> RelayCodec {
        RelayCodec {
            payload_len: 0,
            is_lzip: false,

            knows_pl_len: false,
            knows_compression: false,
        }
    }
}

impl Encoder for RelayCodec {
    type Item = RelayMessage;
    type Error = Error;

    fn encode(&mut self, item: RelayMessage, dst: &mut BytesMut) -> std::io::Result<()> {
        match item {
            RelayMessage::ClientMessage(cmd) => {
                let bytes = cmd.as_bytes();
                dst.extend_from_slice(bytes);
            }
            RelayMessage::RelayMessage { .. } => unreachable!(),
        }
        Ok(())
    }
}

impl Decoder for RelayCodec {
    type Item = RelayMessage;
    type Error = Error;

    fn decode(&mut self, src: &mut BytesMut) -> std::io::Result<Option<RelayMessage>> {
        if !self.knows_pl_len {
            if src.len() < 4 {
                return Ok(None);
            }
            let len_buf = src.split_to(4);
            let pl_len = len_buf.into_buf().get_u32_be();
            self.payload_len = (pl_len - 5) as usize;
            self.knows_pl_len = true;
        }
        if !self.knows_compression {
            if src.len() < 1 {
                return Ok(None);
            }
            let compress_type_buf = src.split_to(1);
            let compress_type = compress_type_buf.into_buf().get_u8();
            self.is_lzip = compress_type == 0x01;
            self.knows_compression = true;
        }
        if src.len() < self.payload_len {
            return Ok(None);
        }
        // Now we shall reset the values of the decoder so
        // stuff doesn't just break!
        self.knows_pl_len = false;
        self.knows_compression = false;
        let pl_buf = src.split_to(self.payload_len);

        let mut pl_read: Box<Read> = if self.is_lzip {
            Box::new(zlib::Decoder::new(&pl_buf[..])?)
        } else {
            Box::new(&pl_buf[..])
        };
        let mut pl_bytes = Vec::new();
        pl_read.read_to_end(&mut pl_bytes);
        parse_msg(&mut std::convert::From::from(pl_bytes)).map(|objs| Some(objs))
    }
}

fn parse_msg(payload_bytes: &mut BytesMut) -> std::io::Result<RelayMessage> {
    let msg_id = parse_string(payload_bytes).unwrap();
    let objs = parse_objs(payload_bytes)?;
    let ret = RelayMessage::RelayMessage {
        id: msg_id,
        objs: objs,
    };
    Ok(ret)
}

fn parse_objs(payload_bytes: &mut BytesMut) -> std::io::Result<Vec<MsgObj>> {
    let mut ret = Vec::new();
    while !payload_bytes.is_empty() {
        let obj_type = parse_obj_type(payload_bytes)?;
        ret.push(parse_obj(payload_bytes, obj_type)?);
    }
    Ok(ret)
}

fn parse_obj_type(bytes: &mut BytesMut) -> std::io::Result<ObjType> {
    let type_id = bytes.split_to(3);
    match &*type_id {
        b"chr" => Ok(ObjType::Char),
        b"int" => Ok(ObjType::Int),
        b"lon" => Ok(ObjType::LongInt),
        b"str" => Ok(ObjType::String),
        b"buf" => Ok(ObjType::Buffer),
        b"ptr" => Ok(ObjType::Pointer),
        b"tim" => Ok(ObjType::Time),
        b"htb" => Ok(ObjType::Hashtable),
        b"hda" => Ok(ObjType::Hdata),
        b"inf" => Ok(ObjType::Info),
        b"inl" => Ok(ObjType::Infolist),
        b"arr" => Ok(ObjType::Array),
        _ => Err(Error::new(ErrorKind::InvalidData, "Invalid type data")),
    }
}

fn parse_obj(bytes: &mut BytesMut, obj_type: ObjType) -> std::io::Result<MsgObj> {
    let ret = match obj_type {
        ObjType::Char => MsgObj::Char(parse_char(bytes)?),
        ObjType::Int => MsgObj::Int(parse_int(bytes)?),
        ObjType::LongInt => MsgObj::LongInt(parse_long_int(bytes)?),
        ObjType::String => MsgObj::String(parse_string(bytes)?),
        ObjType::Buffer => MsgObj::Buffer(parse_string(bytes)?),
        ObjType::Pointer => MsgObj::Pointer(parse_pointer(bytes)?),
        ObjType::Time => MsgObj::Time(parse_time(bytes)?),
        ObjType::Hashtable => MsgObj::Hashtable { pairs: parse_hashtable(bytes)? },
        ObjType::Hdata => {
            let hdata = parse_hdata(bytes)?;
            MsgObj::Hdata {
                hpath: hdata.0,
                objs: hdata.1,
            }
        }
        ObjType::Info => {
            let info = parse_info(bytes)?;
            MsgObj::Info(info.0, info.1)
        }
        ObjType::Infolist => {
            let il = parse_infolist(bytes)?;
            MsgObj::Infolist {
                name: il.0,
                items: il.1,
            }
        }
        ObjType::Array => MsgObj::Array(parse_array(bytes)?),
    };
    Ok(ret)
}

fn parse_char(bytes: &mut BytesMut) -> std::io::Result<char> {
    if bytes.len() < 1 {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }
    let char_buf = bytes.split_to(1);
    Ok(char_buf.into_buf().get_u8() as char)
}

fn parse_int(bytes: &mut BytesMut) -> std::io::Result<i32> {
    if bytes.len() < 4 {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }
    let int_buf = bytes.split_to(4);
    Ok(int_buf.into_buf().get_i32_be())
}

fn parse_long_int(bytes: &mut BytesMut) -> std::io::Result<i64> {
    if bytes.len() < 1 {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }
    let len_buf = bytes.split_to(1);
    let str_len = len_buf.into_buf().get_u8();

    if bytes.len() < str_len as usize {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }

    let str_buf = bytes.split_to(str_len as usize);
    (std::str::from_utf8(&str_buf[..]))
        .map_err(|_| Error::new(ErrorKind::InvalidData, "Long Int Not UTF-8"))
        .and_then(|s| {
            s.parse::<i64>().map_err(|_| {
                Error::new(ErrorKind::InvalidData, "Not parsable as i64")
            })
        })
}

fn parse_string(bytes: &mut BytesMut) -> std::io::Result<Option<String>> {
    if bytes.len() < 4 {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }

    let len_buf = bytes.split_to(4);
    let str_len = len_buf.into_buf().get_u32_be();
    if str_len == std::u32::MAX {
        return Ok(None);
    }
    if str_len == 0 {
        return Ok(Some("".to_string()));
    }
    if bytes.len() < str_len as usize {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }
    let str_buf = bytes.split_to(str_len as usize);
    (std::str::from_utf8(&str_buf[..]))
        .map_err(|_| Error::new(ErrorKind::InvalidData, "String Not UTF-8"))
        .map(|slice| Some(slice.to_string()))
}

fn parse_pointer(bytes: &mut BytesMut) -> std::io::Result<Option<String>> {
    if bytes.len() < 1 {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }

    let len_buf = bytes.split_to(1);
    let len = len_buf.into_buf().get_u8();

    if bytes.len() < len as usize {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }

    let str_buf = bytes.split_to(len as usize);
    if len == 1 && str_buf[0] == '0' as u8 {
        return Ok(None);
    }

    (std::str::from_utf8(&str_buf[..]))
        .map_err(|utf_error| {
            Error::new(
                ErrorKind::InvalidData,
                format!("Pointer Not UTF-8: {}", utf_error),
            )
        })
        .map(|slice| Some(slice.to_string()))
}

fn parse_time(bytes: &mut BytesMut) -> std::io::Result<String> {
    if bytes.len() < 1 {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }

    let len_buf = bytes.split_to(1);
    let len = len_buf.into_buf().get_u8();

    if bytes.len() < len as usize {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }

    let str_buf = bytes.split_to(len as usize);
    (std::str::from_utf8(&str_buf[..]))
        .map_err(|_| Error::new(ErrorKind::InvalidData, "Time Not UTF-8"))
        .map(|slice| slice.to_string())
}

fn parse_hashtable(bytes: &mut BytesMut) -> std::io::Result<Vec<(Box<MsgObj>, Box<MsgObj>)>> {
    if bytes.len() < 10 {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }
    let key_type = parse_obj_type(bytes)?;
    let value_type = parse_obj_type(bytes)?;
    let items_count_buf = bytes.split_to(4);
    let items_count = items_count_buf.into_buf().get_u32_be();
    let mut ret = Vec::new();

    for _ in 0..items_count {
        let key = parse_obj(bytes, key_type)?;
        let value = parse_obj(bytes, value_type)?;
        ret.push((Box::new(key), Box::new(value)));
    }

    Ok(ret)
}

fn parse_hdata(bytes: &mut BytesMut) -> std::io::Result<(Option<String>, Vec<HdataObj>)> {
    let hpath = parse_string(bytes)?;
    let keys_strs: Option<Vec<String>> = parse_string(bytes)?.map(|s| {
        s.split(",").map(|o| o.to_string()).collect()
    });
    let keys_strs: Option<Vec<(String, String)>> = keys_strs.map(|vec| {
        let mut ret = Vec::new();

        for s in vec {
            let mut split_iter = s.split(":");

            while let Some(key) = split_iter.next() {
                // We can be certain that if there is a key,
                // there is a value
                let value = split_iter.next().unwrap();
                ret.push((key.to_string(), value.to_string()));
            }
        }

        ret
    });
    let keys: Option<Vec<(String, ObjType)>> = keys_strs.map(|vec| {
        let mut ret = Vec::new();
        for (key, value) in vec {
            ret.push((
                key.to_string(),
                parse_obj_type(&mut From::from(value)).unwrap(),
            ))
        }
        ret
    });
    if bytes.len() < 4 {
        return Err(Error::new(ErrorKind::UnexpectedEof, "Unexpected EOF"));
    }
    let item_count = parse_int(bytes)?;
    if hpath.is_none() && keys.is_none() && item_count == 0 {
        return Ok((None, Vec::new()));
    }
    let hpath = hpath.unwrap();
    let keys = keys.unwrap();
    let hpath_elems = hpath.split("/").count();
    let mut items = Vec::new();
    for _ in 0..item_count {
        let mut ppath = Vec::new();
        let mut objs = Vec::new();
        for _ in 0..hpath_elems {
            ppath.push(Box::new(parse_obj(bytes, ObjType::Pointer)?));
        }
        for (key_name, vt) in keys.clone() {
            let value = parse_obj(bytes, vt)?;
            let value = Box::new(value);
            objs.push((key_name.clone(), value));
        }
        let hdata_val = HdataObj {
            ppath: ppath,
            values: objs,
        };
        items.push(hdata_val);
    }
    Ok((Some(hpath), items))
}

fn parse_info(bytes: &mut BytesMut) -> std::io::Result<(String, String)> {
    let name = parse_string(bytes)?.unwrap();
    let value = parse_string(bytes)?.unwrap();
    Ok((name, value))
}

fn parse_infolist(
    bytes: &mut BytesMut,
) -> std::io::Result<(String, Vec<Vec<(String, Box<MsgObj>)>>)> {
    let name = parse_string(bytes)?.unwrap();
    let item_count = parse_int(bytes)?;
    let mut ret = Vec::new();

    for _ in 0..item_count {
        let val_count = parse_int(bytes)?;
        let mut item = Vec::new();
        for _ in 0..val_count {
            let val_name = parse_string(bytes)?.unwrap();
            let val_type = parse_obj_type(bytes)?;
            let val = Box::new(parse_obj(bytes, val_type)?);
            item.push((val_name, val));
        }
        ret.push(item);
    }

    Ok((name, ret))
}

fn parse_array(bytes: &mut BytesMut) -> std::io::Result<Option<Vec<Box<MsgObj>>>> {
    let elem_type = parse_obj_type(bytes)?;
    let arr_len = parse_int(bytes)?;
    if arr_len == 0 {
        return Ok(None);
    }
    let mut ret = Vec::new();
    for _ in 0..arr_len {
        ret.push(Box::new(parse_obj(bytes, elem_type)?));
    }
    Ok(Some(ret))
}

#[derive(Clone)]
struct ClientCommand {
    id: Option<String>,
    command: String,
    args: Vec<String>,
}

pub struct ClientCommandBuilder {
    cmds: Vec<ClientCommand>,
    curr_command: ClientCommand,
}

impl ClientCommandBuilder {
    pub fn new() -> Self {
        ClientCommandBuilder {
            cmds: Vec::new(),
            curr_command: ClientCommand {
                id: None,
                command: "".to_string(),
                args: Vec::new(),
            },
        }
    }

    pub fn new_command<T>(self, cmd: T) -> Self
    where
        T: Into<String>,
    {
        let mut cmds = self.cmds.to_vec();
        if self.curr_command.command != "" {
            cmds.push(self.curr_command);
        }
        ClientCommandBuilder {
            cmds: cmds,
            curr_command: ClientCommand {
                id: None,
                command: cmd.into(),
                args: Vec::new(),
            },
        }
    }

    pub fn set_command_id<T>(self, id: T) -> Self
    where
        T: Into<String>,
    {
        ClientCommandBuilder {
            cmds: self.cmds,
            curr_command: ClientCommand {
                id: Some(id.into()),
                command: self.curr_command.command,
                args: self.curr_command.args,
            },
        }
    }

    pub fn unset_command_id(self) -> Self {
        ClientCommandBuilder {
            cmds: self.cmds,
            curr_command: ClientCommand {
                id: None,
                command: self.curr_command.command,
                args: self.curr_command.args,
            },
        }
    }

    pub fn add_command_arg<T>(self, arg: T) -> Self
    where
        T: Into<String>,
    {
        let mut new_vec = self.curr_command.args;
        new_vec.push(arg.into());
        ClientCommandBuilder {
            cmds: self.cmds,
            curr_command: ClientCommand {
                id: self.curr_command.id,
                command: self.curr_command.command,
                args: new_vec,
            },
        }
    }

    pub fn remove_command_arg(self) -> Self {
        let mut new_vec = self.curr_command.args;
        new_vec.pop();
        ClientCommandBuilder {
            cmds: self.cmds,
            curr_command: ClientCommand {
                id: self.curr_command.id,
                command: self.curr_command.command,
                args: new_vec,
            },
        }
    }

    pub fn build(self) -> RelayMessage {
        let create_cmdline = |command: ClientCommand| {
            let id_prefix = command.id.into_iter().fold(
                "".to_string(),
                |_, s| format!("({}) ", s),
            );
            let args = command.args.into_iter().fold("".to_string(), |acc, s| {
                format!("{} {}", acc, s)
            });
            let ret = format!("{}{}{}\n", id_prefix, command.command, args);
            ret.trim();
            ret
        };
        let curr_cmd_iter = std::iter::once(self.curr_command);
        let prev_cmds_iter = self.cmds.into_iter();
        let ret = prev_cmds_iter
            .chain(curr_cmd_iter)
            .map(create_cmdline)
            .fold("".to_string(), |acc, s| format!("{}{}", acc, s));
        RelayMessage::ClientMessage(ret)
    }
}
